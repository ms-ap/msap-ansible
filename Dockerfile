#FROM python:3.6.5-stretch
FROM ubuntu:18.04

RUN apt-get update -qy && \
  apt-get install -y  sudo \
                      python \
                      python-pip && \
  apt-get clean

RUN pip install --upgrade pip
#RUN apt-get install python3-apt
#RUN pip install --upgrade pycrypto cryptography
RUN pip install ansible==2.7.*