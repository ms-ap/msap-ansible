kube-master
=========

Install a machine as a kubernetes master. This includes:
- setting 'KUBECONFIG=/etc/kubernetes/admin.conf' as environment variable
- initializing a kubernetes cluster with kubeadm
- installing a CNI (Container Network Interface)
- displaying the join command in the motd
- create a join.sh script which can be pushed to clients, to let them join the cluster

Requirements
------------

None

Role Variables
--------------

- kubernetes.cni
  - The CNI which should be installed
  - One of {calico, canal, flannel, kube-router, romana, weavenet}
- kubernetes.cri
  - The CRI which should be used
  - One of {docker, crio, containerd}
- kubernetes.enable_dashboard
  - True: kubernetes dashboard should be enabled
  - False: kubernetes dashboard should not be enabled
- kubernetes.dashboard_port
  - Port on which the dashboard should be exposed (must be between 30000 and 32767)
- kubernetes.config
  - List of paths, users, groups which should habe access to the kubernetes config
- kubernetes.version
  - Version of kubernetes
- deps
  - If set to false the dependencies will be skipped

Dependencies
------------

- cri (docker, crio, containerd)
- kubeadm

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
        - kubeadm
        - kube-master
      vars:
        - kubernetes:
            apiserver_advertise_address: 192.168.90.2
            cni: weavenet
            config:
            - {group: root, home: /root, user: root}
            - {group: vagrant, home: /home/vagrant/, user: vagrant}
            cri: docker
            cri_settings:
              containerd: {version: 1.2.1}
              crio: {version: 1.12, yum_repo: 'https://cbs.centos.org/repos/paas7-crio-311-candidate/x86_64/os/'}
              docker: {dest: /tmp/docker-installer.sh, url: 'https://get.docker.com/', version: 18.06.1}
            custom_node_ip: true
            enable_dashboard: true
            kube_proxy_port: 8001
            version: 1.13.0