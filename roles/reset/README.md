reset
=========

This role is used to reset kubeadm.

Requirements
------------

- Cluster should be initialized (otherwise resetting makes no sense)

Role Variables
--------------

- master
  - If set to True kubectl proxy will be killed

Dependencies
------------

- None

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - { role: reset, master: True }
    - hosts: nodes
        - reset
