containerd
=========

Installs containerd.

Requirements
------------

None

Role Variables
--------------

- cri_settings.containerd.version
  - version of containerd

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - containerd
      vars:
        - cri_settings:
            containerd:
              version: 1.1.0