kubeadm
=========

This role is used to install kubeadm, kubelet and kubectl on the system.

Requirements
------------

None

Role Variables
--------------

- kubernetes.version
  - Specified version of kubelet, kubeadm and kubectl will be installed
- kubernetes.custom_node_ip
  - If set to true kubelet will be started with --node-ip=subnet.IP command
  - Only useful with some multi-nic hosts, where kubeadm chooses the wrong ip
- subnet: 192.168.90
  - Subnet of the kubernetes cluster IP-Address will be used
  - Only useful with some multi-nic hosts, where kubeadm chooses the wrong ip

Dependencies
------------

- docker

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
        - kubeadm
      vars:
        - kubernetes:
            version: 1.13.0
            custom_node_ip: false
            cri: docker
        - subnet: 192.168.90