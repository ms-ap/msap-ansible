swarm-master
=========

Install a machine as a swarm manager

Requirements
------------

None

Role Variables
--------------

- swarm.master_advertise_interface
  - Sets the interface of the advertise address
- deps
  - If set to false the dependencies will be skipped

Dependencies
------------

- docker

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
        - swarm-master
      vars:
        - swarm:
            master_advertise_interface: eth0