common
=========

Common tasks, that should be executed on every host (i. e. setting timezone, language etc.)

Requirements
------------

None

Role Variables
--------------

- timezone: Timezone in the form "Africa/Abidjan" see https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
- additional_apt_packages: A list of additional apt packages that should be installed
- additional_yum_packages: A list of additional yum packages that should be installed

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - common
      vars:
        timezone: Europe/Berlin
        additional_apt_packages:
          - nfs-common