kube-node
=========

Push the join.sh script which was created by the master role to a node.

Requirements
------------

- master role must be executed first, to create the join.sh script or script must be manually created in `/etc/kubernetes/join.sh`

Role Variables
--------------

- deps
  - If set to false the dependencies will be skipped

Dependencies
------------

- docker
- kubeadm

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
        - kubeadm
        - kube-node