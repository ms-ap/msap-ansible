packer
=========

This role should be used to provision a packer created box with all needed files to speed up installation in vagrant environments.
If used in a playbook it should be started with ´ansible-playbook --skip-tags=swap´ to avoid errors.

Requirements
------------

None

Role Variables
--------------

- kubernetes_version
  - version of kubernetes for which the docker images should be pulled

Dependencies
------------

- docker
- kubeadm

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
        - kubeadm
        - packer
      vars:
        - kubernetes:
            version: 1.11.0