handlers
=========

Handlers that should be accessible from every role.

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

None