swarm-node
=========

Join a swarm.

Requirements
------------

- master role must be executed first to initialize the swarm.

Role Variables
--------------

- swarm.node_advertise_interface
  - Sets the interface of the advertise address
- deps
  - If set to false the dependencies will be skipped

Dependencies
------------

- docker

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
        - swarm-node
      vars:
        - swarm:
            node_advertise_interface: eth0