cri-o
=====

Installs cri-o.

Requirements
------------

None

Role Variables
--------------

- cri_settings.crio.version
  - version of cri-o
- cri_settings.crio.yum_repo
  - yum repo for crio

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - crio
      vars:
        - cri_settings:
            crio:
              version: 1.12
              yum_repo: https://cbs.centos.org/repos/paas7-crio-311-candidate/x86_64/os/