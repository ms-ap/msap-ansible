docker
=========

Installs docker.

Requirements
------------

None

Role Variables
--------------

- docker_installer.url
  - The url where the installer script lives
- docker_installer.dest: /tmp/docker-installer.sh
  - temporary directory on the remote host to download the installer to
- docker_installer.sha256: A7C49D9B3E199633C2A48176420EE1365F39A5EAAC516E7AEEB1E04ABEE90B0D
  - sha256 sum of the installer file to avoid spoofing (has to be updated, when the installer gets updated)
- docker_installer.version: 18.05.0-ce
   - specific docker version which should be intalled

Dependencies
------------

None

Example Playbook
----------------

    - hosts: servers
      become: yes
      roles:
        - docker
      vars:
        - cri_settings
            docker:
              url: https://get.docker.com/
              dest: /tmp/docker-installer.sh
              version: 18.06.0-ce